package nguyenchuc.captain.appandroid;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.text.DecimalFormat;

public class ThemSanPhamDialog extends DialogFragment {

    ImageView imageView ;
    TextView textViewTen ;
    TextView textViewGia ;
    TextView textViewSoLuong;
    Button buttonTru;
    Button buttonso;
    Button buttoncong;
    Button buttonThem ;
    Button buttonHuy;

    //nhận giá trị tên, giá bán, hình ảnh sản phẩm khi khởi tạo dialog
    public static ThemSanPhamDialog newInstance(int id, String ten,Integer giaNiemYet, Integer gia, String hinhAnh, String mota, Integer daban, int idLoaiSP) {
        ThemSanPhamDialog dialog = new ThemSanPhamDialog();
        Bundle bundle = new Bundle();
        bundle.putInt("id",id);
        bundle.putString("ten", ten);
        bundle.putInt("giaNiemYet",giaNiemYet);
        bundle.putInt("gia", gia);
        bundle.putString("hinhAnh", hinhAnh);
        bundle.putString("mota",mota);
        bundle.putInt("daban",daban);
        bundle.putInt("idLoaiSP",idLoaiSP);
        dialog.setArguments(bundle);
        return dialog;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.dialog_them_san_pham,container);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        // lấy giá trị từ bundle
        String ten = getArguments().getString("ten", "");
        Integer gia = getArguments().getInt("gia",0);
        String hinhAnh = getArguments().getString("hinhAnh", "");

        imageView = (ImageView) view.findViewById(R.id.imageviewDialog);
        textViewTen = (TextView) view.findViewById(R.id.textViewTenDialog);
        textViewGia = (TextView) view.findViewById(R.id.textViewGiaBanDialog);
        textViewSoLuong = (TextView) view.findViewById(R.id.textViewSoLuongDialog);
        buttonTru = (Button) view.findViewById(R.id.buttonTruDialog);
        buttonso = (Button) view.findViewById(R.id.buttonSoDialog);
        buttoncong = (Button) view.findViewById(R.id.buttonCongDialog);
        buttonThem = (Button) view.findViewById(R.id.buttonThemDialog);
        buttonHuy = (Button) view.findViewById(R.id.buttonHuyDialog);

        textViewTen.setText(ten);
        DecimalFormat decimalFormat = new DecimalFormat("###,###,###"); //định dạng giá cả
        textViewGia.setText(decimalFormat.format(gia)+" đ");
        Picasso.with(getActivity()).load(hinhAnh)
                .placeholder(R.drawable.loading)
                .error(R.drawable.eror)
                .into(imageView);
        imageView.setScaleType(ImageView.ScaleType.FIT_XY);

        buttonTru.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int so = Integer.parseInt(buttonso.getText().toString());
                if(so>1){
                    so-=1;
                    buttonso.setText(so+"");
                }else {
                    getDialog().cancel();
                }
            }
        });

        buttoncong.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int so = Integer.parseInt(buttonso.getText().toString())+1;
                buttonso.setText(so+"");
            }
        });


        buttonThem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getDialog().cancel();

                int t=Integer.parseInt(buttonso.getText().toString());
                Intent intent = new Intent(getActivity(),GioHangActivity.class);
                intent.putExtra("idGio",getArguments().getInt("id",0));
                intent.putExtra("tenGio",getArguments().getString("ten", ""));
                intent.putExtra("giaNiemYetGio",getArguments().getInt("giaNiemYet",0));
                intent.putExtra("giaBanGio",getArguments().getInt("gia",0));
                intent.putExtra("hinhAnhGio",getArguments().getString("hinhAnh", ""));
                intent.putExtra("motaGio",getArguments().getString("mota", ""));
                intent.putExtra("daBanGio",getArguments().getInt("daban",0));
                intent.putExtra("idLoaiSPGio",getArguments().getInt("idLoaiSP",0));
                intent.putExtra("soluongGio",t);
                startActivity(intent);
            }
        });
        buttonHuy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getDialog().cancel();
            }
        });
    }
}
