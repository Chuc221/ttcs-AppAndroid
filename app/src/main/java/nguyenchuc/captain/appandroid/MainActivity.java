package nguyenchuc.captain.appandroid;

import android.app.Application;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;
import android.widget.ViewFlipper;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class MainActivity extends AppCompatActivity {

    DrawerLayout drawerLayout;
    Toolbar toolbar;
    ViewFlipper viewFlipper;
    GridView gridView;
    NavigationView navigationView;
    ListView listView;

    //tạo mảng
    ArrayList<quangcao> mangQC;

    ArrayList<LoaiDienThoai> arrayLoaiDienThoai;
    LoaiDienThoaiAdapter loaiDienThoaiAdapter;

    ArrayList<DienThoai> arrayListDienThoai;
    SanPhamMoiAdapter sanPhamMoiAdapter;

    static KhachHang khachHang;

    int sumLoaiDT;

    int id;
    String ten;
    Integer giaNiemYet;
    Integer giaBan;
    String hinhAnh;
    String moTa;
    Integer daBan;
    int idLoaiDienThoai;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //ánh xạ các chức năng
        drawerLayout = (DrawerLayout) findViewById(R.id.drawerLayoutManHinhChinh);
        toolbar = (Toolbar) findViewById(R.id.toolbarManHinhChinh);
        viewFlipper = (ViewFlipper) findViewById(R.id.viewFlipperManHinhChinh);
        gridView = (GridView) findViewById(R.id.gridViewManHinhChinh);
        navigationView = (NavigationView) findViewById(R.id.navigationViewManHinhChinh);
        listView = (ListView) findViewById(R.id.listviewManHinhChinh);

        mangQC = new ArrayList<>();

        arrayLoaiDienThoai = new ArrayList<>();
        loaiDienThoaiAdapter = new LoaiDienThoaiAdapter(getApplicationContext(), arrayLoaiDienThoai);
        listView.setAdapter(loaiDienThoaiAdapter);

        arrayListDienThoai = new ArrayList<>();
        sanPhamMoiAdapter = new SanPhamMoiAdapter(getApplicationContext(),arrayListDienThoai);
        gridView.setAdapter(sanPhamMoiAdapter);



        if(ktketnoi.haveNetworkConnection(getApplicationContext())){

            getKhachHang();
            chayViewFlipper();
            menu();
            getLoaiDienThoai();
            chonMenu();
            getDienThoai();
            chonSanPham();

        }
        else {

            ktketnoi.ShowToast_Short(getApplicationContext(),"Vui lòng kiểm tra lại kết nối!");
            finish();

        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.menu,menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menuGioHang:
                Intent intent = new Intent(getApplicationContext(),GioHangActivity.class);
                intent.putExtra("idGio",0);
                intent.putExtra("tenGio","0");
                intent.putExtra("giaNiemYetGio",0);
                intent.putExtra("giaBanGio",0);
                intent.putExtra("hinhAnhGio","0");
                intent.putExtra("motaGio","0");
                intent.putExtra("daBanGio",0);
                intent.putExtra("idLoaiSPGio",0);
                intent.putExtra("soluongGio",0);
                startActivity(intent);
                break;

            case R.id.menuTang:
                Collections.sort(arrayListDienThoai, new Comparator<DienThoai>() {
                    @Override
                    public int compare(DienThoai t, DienThoai t1) {
                        if (t.getGiaBan()> t1.getGiaBan()){
                            return 1;
                        }
                        if (t.getGiaBan() == t1.getGiaBan()){
                            if (t.getDaBan()< t1.getDaBan()){
                                return 1;
                            }
                        }
                        return -1;
                    }
                });
                sanPhamMoiAdapter.notifyDataSetChanged();
                break;

            case R.id.menuGiam:
                Collections.sort(arrayListDienThoai, new Comparator<DienThoai>() {
                    @Override
                    public int compare(DienThoai t, DienThoai t1) {
                        if (t.getGiaBan()< t1.getGiaBan()){
                            return 1;
                        }
                        if (t.getGiaBan() == t1.getGiaBan()){
                            if (t.getDaBan()< t1.getDaBan()){
                                return 1;
                            }
                        }
                        return -1;
                    }
                });
                sanPhamMoiAdapter.notifyDataSetChanged();
                break;

            case R.id.menuPhoBien:
                Collections.sort(arrayListDienThoai, new Comparator<DienThoai>() {
                    @Override
                    public int compare(DienThoai t, DienThoai t1) {
                        if (t.getDaBan()< t1.getDaBan()){
                            return 1;
                        }
                        if (t.getDaBan() == t1.getDaBan()){
                            if (t.getGiaBan()< t1.getGiaBan()){
                                return 1;
                            }
                        }
                        return -1;
                    }
                });
                sanPhamMoiAdapter.notifyDataSetChanged();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void chayViewFlipper(){ //lấy các hình ảnh quảng cáo, sale,... để hiển thị trên trang chủ

        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(ketnoicsdl.duongDanHinhAnhQC, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {

                if (response != null) {

                    int id=0;
                    String hinhAnhQC="";
                    for (int i = 0; i < response.length(); i++) {
                        try {

                            JSONObject jsonObject = response.getJSONObject(i);
                            id=jsonObject.getInt("id");
                            hinhAnhQC=jsonObject.getString("hinhanhquangcao");
                            mangQC.add(new quangcao(id,hinhAnhQC));

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    for (int i = 0; i < mangQC.size(); i++) {
                        ImageView imageView=new ImageView(getApplicationContext());
                        Picasso.with(getApplicationContext()).load(mangQC.get(i).getHinhAnhQC()).into(imageView);
                        imageView.setScaleType(ImageView.ScaleType.FIT_XY);
                        viewFlipper.addView(imageView);
                    }

                    viewFlipper.setFlipInterval(3000);
                    viewFlipper.setAutoStart(true);

                    Animation animationQCin = AnimationUtils.loadAnimation(getApplicationContext(),R.anim.qc_in_left);
                    Animation animationQCout = AnimationUtils.loadAnimation(getApplicationContext(),R.anim.qc_out_left);
                    viewFlipper.setInAnimation(animationQCin);
                    viewFlipper.setOutAnimation(animationQCout);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                ktketnoi.ShowToast_Short(getApplicationContext(),error.toString());
            }
        });
        requestQueue.add(jsonArrayRequest);

    }

    private void getLoaiDienThoai(){  //lấy dữ liệu các loại điện thoại để cho vào thanh menu
        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(ketnoicsdl.loaiDienThoai, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {

                sumLoaiDT=response.length();

                if(response!= null){

                    int id=0;
                    String ten="";
                    String hinhAnh="";

                    arrayLoaiDienThoai.add(0,new LoaiDienThoai(0,"Trang chủ","https://e7.pngegg.com/pngimages/679/69/png-clipart-home-assistant-computer-icons-home-automation-kits-amazon-echo-home-blue-logo-thumbnail.png"));

                    //Thêm các loại điện thoại vào mảng
                    for (int i = 0; i < response.length(); i++) {
                        try {

                            JSONObject jsonObject = response.getJSONObject(i);
                            id = jsonObject.getInt("id");
                            ten = jsonObject.getString("tenLoaiDienThoai");
                            hinhAnh = jsonObject.getString("hinhAnh");
                            arrayLoaiDienThoai.add(new LoaiDienThoai(id,ten,hinhAnh));
                            loaiDienThoaiAdapter.notifyDataSetChanged();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    arrayLoaiDienThoai.add(8,new LoaiDienThoai(8,"Liên Hệ","https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcR7mbN_a9n1sWseU11XkJJ95CqcOnbHOypZEA&usqp=CAU"));
                    arrayLoaiDienThoai.add(9, new LoaiDienThoai(9,"Thông tin","https://png.pngtree.com/png-vector/20190215/ourlarge/pngtree-vector-valid-user-icon-png-image_516022.jpg"));

                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                ktketnoi.ShowToast_Short(getApplicationContext(), error.toString());
            }
        });

        requestQueue.add(jsonArrayRequest);

    }

    private void menu(){  //hiển thị thanh menu các loại điện thoại
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationIcon(android.R.drawable.ic_menu_sort_by_size);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                drawerLayout.openDrawer(GravityCompat.START);
            }
        });
    }

    private void getDienThoai(){ //lấy dữ liệu các sản phẩm mới để đổ ra màn hình
        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(ketnoicsdl.sanPhamMoi, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                if (response!=null){
                    for (int i = 0; i < response.length(); i++) {
                        try {
                            JSONObject jsonObject = response.getJSONObject(i);
                            id = jsonObject.getInt("id");
                            ten = jsonObject.getString("tenDienThoai");
                            giaNiemYet = jsonObject.getInt("giaNiemYet");
                            giaBan = jsonObject.getInt("giaBan");
                            hinhAnh = jsonObject.getString("hinhAnh");
                            moTa = jsonObject.getString("moTa");
                            daBan = jsonObject.getInt("daBan");
                            idLoaiDienThoai = jsonObject.getInt("idLoaiDienThoai");

                            arrayListDienThoai.add(new DienThoai(id,ten, giaNiemYet, giaBan, hinhAnh, moTa, daBan, idLoaiDienThoai));
                            sanPhamMoiAdapter.notifyDataSetChanged();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                ktketnoi.ShowToast_Short(getApplicationContext(), error.toString());
            }
        });
        requestQueue.add(jsonArrayRequest);
    }

    private void chonMenu(){ //chọn loại điện thoại trong menu

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                if(i ==0){
                    if (ktketnoi.haveNetworkConnection(getApplicationContext())){

                        Intent intent = new Intent(MainActivity.this,MainActivity.class);
                        intent.putExtra("idkh",khachHang.getIdkh());
                        intent.putExtra("username",khachHang.getUsername());
                        intent.putExtra("password",khachHang.getPassword());
                        intent.putExtra("hoten",khachHang.getHoten());
                        intent.putExtra("diachi",khachHang.getDiachi());
                        intent.putExtra("sodt",khachHang.getSodt());
                        startActivity(intent);

                    }else {

                        ktketnoi.ShowToast_Short(getApplicationContext(),"Vui lòng kiểm tra lại kết nối!");

                    }
                    drawerLayout.closeDrawer(GravityCompat.START);
                }
                if(i>=1 && i<=sumLoaiDT){
                    if (ktketnoi.haveNetworkConnection(getApplicationContext())){

                        Intent intent = new Intent(MainActivity.this,SanPhamActivity.class);
                        intent.putExtra("idLoaiDT",arrayLoaiDienThoai.get(i).getId());
                        intent.putExtra("logo",arrayLoaiDienThoai.get(i).getHinhAnh());
                        intent.putExtra("tenLoaiDT",arrayLoaiDienThoai.get(i).getTenLoaiDienThoai());
                        startActivity(intent);

                    }else {

                        ktketnoi.ShowToast_Short(getApplicationContext(),"Vui lòng kiểm tra lại kết nối!");

                    }
                    drawerLayout.closeDrawer(GravityCompat.START);
                }
                if(i==(sumLoaiDT+1)){
                    if (ktketnoi.haveNetworkConnection(getApplicationContext())){

                        Intent intent = new Intent(getApplicationContext(),LienHeActivity.class);
                        startActivity(intent);

                    }else {

                        ktketnoi.ShowToast_Short(getApplicationContext(),"Vui lòng kiểm tra lại kết nối!");

                    }
                    drawerLayout.closeDrawer(GravityCompat.START);
                }
                if (i==(sumLoaiDT+2)){
                    if (ktketnoi.haveNetworkConnection(getApplicationContext())){

                        Intent intent = new Intent(getApplicationContext(),ThongTinActivity.class);
                        startActivity(intent);

                    }else {
                        ktketnoi.ShowToast_Short(getApplicationContext(),"Vui lòng kiểm tra lại kết nối!");
                    }
                    drawerLayout.closeDrawer(GravityCompat.START);
                }

            }
        });

    }

    private void chonSanPham(){
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent intent = new Intent(getApplicationContext(),ThongTinChiTietActivity.class);
                intent.putExtra("idChiTiet",arrayListDienThoai.get(i).getId());
                intent.putExtra("tenChiTiet",arrayListDienThoai.get(i).getTen());
                intent.putExtra("giaNiemYetChiTiet",arrayListDienThoai.get(i).getGiaNiemYet());
                intent.putExtra("giaBanChiTiet",arrayListDienThoai.get(i).getGiaBan());
                intent.putExtra("hinhAnhChiTiet",arrayListDienThoai.get(i).getHinhAnh());
                intent.putExtra("motaChiTiet",arrayListDienThoai.get(i).getMoTa());
                intent.putExtra("daBanChiTiet",arrayListDienThoai.get(i).getDaBan());
                intent.putExtra("idLoaiSPChiTiet",arrayListDienThoai.get(i).getIdLoaiDienThoai());
                startActivity(intent);
            }
        });
    }

    private void getKhachHang(){
        khachHang = new KhachHang(getIntent().getIntExtra("idkh",0),
                getIntent().getStringExtra("username"),
                getIntent().getStringExtra("password"),
                getIntent().getStringExtra("hoten"),
                getIntent().getStringExtra("diachi"),
                getIntent().getStringExtra("sodt")
                );
    }

}

