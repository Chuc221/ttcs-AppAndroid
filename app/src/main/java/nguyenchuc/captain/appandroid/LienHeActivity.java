package nguyenchuc.captain.appandroid;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class LienHeActivity extends AppCompatActivity {

    DrawerLayout drawerLayoutLienHe;
    Toolbar toolbarLienHe;
    NavigationView navigationViewLienHe;
    ListView listViewLoaiSP;

    ArrayList<LoaiDienThoai> arrayLoaiDienThoai;
    LoaiDienThoaiAdapter loaiDienThoaiAdapter;
    int sumLoaiDT;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lien_he);

        drawerLayoutLienHe = (DrawerLayout) findViewById(R.id.drawerLayoutLienHe);
        toolbarLienHe = (Toolbar) findViewById(R.id.toolbarLienHe);
        navigationViewLienHe = (NavigationView) findViewById(R.id.navigationViewLienHe);
        listViewLoaiSP = (ListView) findViewById(R.id.listviewLoaiSanPhamLienHe);

        arrayLoaiDienThoai = new ArrayList<>();
        loaiDienThoaiAdapter = new LoaiDienThoaiAdapter(getApplicationContext(), arrayLoaiDienThoai);
        listViewLoaiSP.setAdapter(loaiDienThoaiAdapter);

        if(ktketnoi.haveNetworkConnection(getApplicationContext())){

            menu();
            getLoaiDienThoai();
            chonMenu();

        }else {
            ktketnoi.ShowToast_Short(getApplicationContext(),"Vui lòng kiểm tra lại kết nối!");
            finish();
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.menu_rieng,menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menuGioHang:
                Intent intent = new Intent(getApplicationContext(),GioHangActivity.class);
                intent.putExtra("idGio",0);
                intent.putExtra("tenGio","0");
                intent.putExtra("giaNiemYetGio",0);
                intent.putExtra("giaBanGio",0);
                intent.putExtra("hinhAnhGio","0");
                intent.putExtra("motaGio","0");
                intent.putExtra("daBanGio",0);
                intent.putExtra("idLoaiSPGio",0);
                intent.putExtra("soluongGio","0");
                startActivity(intent);
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void chonMenu(){ //chọn loại điện thoại trong menu

        listViewLoaiSP.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                if(i ==0){
                    if (ktketnoi.haveNetworkConnection(getApplicationContext())){

                        Intent intent = new Intent(getApplicationContext(),MainActivity.class);
                        intent.putExtra("idkh",MainActivity.khachHang.getIdkh());
                        intent.putExtra("username",MainActivity.khachHang.getUsername());
                        intent.putExtra("password",MainActivity.khachHang.getPassword());
                        intent.putExtra("hoten",MainActivity.khachHang.getHoten());
                        intent.putExtra("diachi",MainActivity.khachHang.getDiachi());
                        intent.putExtra("sodt",MainActivity.khachHang.getSodt());
                        startActivity(intent);

                    }else {

                        ktketnoi.ShowToast_Short(getApplicationContext(),"Vui lòng kiểm tra lại kết nối!");

                    }
                    drawerLayoutLienHe.closeDrawer(GravityCompat.START);
                }
                if(i>=1 && i<=sumLoaiDT){
                    if (ktketnoi.haveNetworkConnection(getApplicationContext())){

                        Intent intent = new Intent(getApplicationContext(),SanPhamActivity.class);
                        intent.putExtra("idLoaiDT",arrayLoaiDienThoai.get(i).getId());
                        intent.putExtra("logo",arrayLoaiDienThoai.get(i).getHinhAnh());
                        intent.putExtra("tenLoaiDT",arrayLoaiDienThoai.get(i).getTenLoaiDienThoai());
                        startActivity(intent);

                    }else {

                        ktketnoi.ShowToast_Short(getApplicationContext(),"Vui lòng kiểm tra lại kết nối!");

                    }
                    drawerLayoutLienHe.closeDrawer(GravityCompat.START);
                }
                if(i==(sumLoaiDT+1)){
                    if (ktketnoi.haveNetworkConnection(getApplicationContext())){

                        Intent intent = new Intent(getApplicationContext(),LienHeActivity.class);
                        startActivity(intent);

                    }else {

                        ktketnoi.ShowToast_Short(getApplicationContext(),"Vui lòng kiểm tra lại kết nối!");

                    }
                    drawerLayoutLienHe.closeDrawer(GravityCompat.START);
                }
                if (i==(sumLoaiDT+2)){
                    if (ktketnoi.haveNetworkConnection(getApplicationContext())){

                        Intent intent = new Intent(getApplicationContext(),ThongTinActivity.class);
                        startActivity(intent);

                    }else {
                        ktketnoi.ShowToast_Short(getApplicationContext(),"Vui lòng kiểm tra lại kết nối!");
                    }
                    drawerLayoutLienHe.closeDrawer(GravityCompat.START);
                }

            }
        });

    }

    private void getLoaiDienThoai(){  //lấy dữ liệu các loại điện thoại để cho vào thanh menu
        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(ketnoicsdl.loaiDienThoai, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {

                sumLoaiDT=response.length();

                if(response!= null){

                    int id=0;
                    String ten="";
                    String hinhAnh="";

                    arrayLoaiDienThoai.add(0,new LoaiDienThoai(0,"Trang chủ","https://e7.pngegg.com/pngimages/679/69/png-clipart-home-assistant-computer-icons-home-automation-kits-amazon-echo-home-blue-logo-thumbnail.png"));

                    //Thêm các loại điện thoại vào mảng
                    for (int i = 0; i < response.length(); i++) {
                        try {

                            JSONObject jsonObject = response.getJSONObject(i);
                            id = jsonObject.getInt("id");
                            ten = jsonObject.getString("tenLoaiDienThoai");
                            hinhAnh = jsonObject.getString("hinhAnh");
                            arrayLoaiDienThoai.add(new LoaiDienThoai(id,ten,hinhAnh));
                            loaiDienThoaiAdapter.notifyDataSetChanged();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    arrayLoaiDienThoai.add(8,new LoaiDienThoai(8,"Liên Hệ","https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcR7mbN_a9n1sWseU11XkJJ95CqcOnbHOypZEA&usqp=CAU"));
                    arrayLoaiDienThoai.add(9, new LoaiDienThoai(9,"Thông tin","https://png.pngtree.com/png-vector/20190215/ourlarge/pngtree-vector-valid-user-icon-png-image_516022.jpg"));

                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                ktketnoi.ShowToast_Short(getApplicationContext(), error.toString());
            }
        });

        requestQueue.add(jsonArrayRequest);

    }

    private void menu(){  //hiển thị thanh menu các loại điện thoại
        setSupportActionBar(toolbarLienHe);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbarLienHe.setNavigationIcon(android.R.drawable.ic_menu_sort_by_size);
        toolbarLienHe.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                drawerLayoutLienHe.openDrawer(GravityCompat.START);
            }
        });
    }
}